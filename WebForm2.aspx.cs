﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Data.OleDb;
using System.Configuration;

namespace WebApplicationtest
{
    public partial class WebForm2 : System.Web.UI.Page
    {

        //string mainconn = ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString;

        protected System.Web.UI.HtmlControls.HtmlInputFile File1;
        protected System.Web.UI.HtmlControls.HtmlInputButton Submit1;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getDevice();            
            }

        }



        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(
            "Data Source=ARIFPC;Initial Catalog=can_support;Integrated Security=True");
            con.Open();



            SqlCommand cmd = new SqlCommand("insert into test(device)values('" + inputQuestion.InnerText + "')", con);
            int i = cmd.ExecuteNonQuery();

            if (i != 0)
            {
                Label1.Text = Request.Form["inputQuestion"];
            }
            else
            {

            }
            con.Close();
        }


        protected void Buttonuplo_Click(object sender, EventArgs e)
        {
            if ((FileUpload1.PostedFile != null) && (FileUpload1.PostedFile.ContentLength > 0))
            {
                string fn = System.IO.Path.GetFileName(FileUpload1.PostedFile.FileName);
                string SaveLocation = Server.MapPath("upload") + "\\" + fn;
                try
                {
                    FileUpload1.PostedFile.SaveAs(SaveLocation);
                    FileUploadStatus.Text = "The file has been uploaded.";
                }
                catch (Exception ex)
                {
                    FileUploadStatus.Text = "Error: " + ex.Message;
                }
            }
            else
            {
                FileUploadStatus.Text = "Please select a file to upload.";
            }
        }
        

        protected void getDevice()
        {

            SqlConnection sqlcon = new SqlConnection("Data Source=ARIFPC;Initial Catalog=can_support;Integrated Security=True");
            sqlcon.Open();
            SqlCommand sqlcomm = new SqlCommand("select * from device",sqlcon);
            sqlcomm.CommandType = CommandType.Text;
            DropDownDevice.DataSource = sqlcomm.ExecuteReader();
            DropDownDevice.DataTextField = "de_name";
            DropDownDevice.DataValueField = "de_id";
            DropDownDevice.DataBind();
            DropDownDevice.Items.Insert(0, "Select Devices");
            DropDownDevice.Items[0].Value = "0";
        }


        protected void DropDownDevice_SelectedIndexChanged(object sender, EventArgs e)
        {
            int de_id = Convert.ToInt32(DropDownDevice.SelectedValue);
            SqlConnection sqlcon = new SqlConnection("Data Source=ARIFPC;Initial Catalog=can_support;Integrated Security=True");
            sqlcon.Open();
            SqlCommand sqlcomm = new SqlCommand("SELECT module.mo_name FROM select_device INNER JOIN device ON select_device.de_id = device.de_id  LEFT JOIN module ON select_device.mo_id = module.mo_id where device.de_id =" + de_id, sqlcon);

            sqlcomm.CommandType = CommandType.Text;
            DropDownModule.DataSource = sqlcomm.ExecuteReader();
            DropDownModule.DataTextField = "mo_name";
            //DropDownModule.DataValueField = "sd_id";
            DropDownModule.DataBind();
            DropDownModule.Items.Insert(0, "Select Module");
            DropDownModule.Items[0].Value = "0";
            sqlcon.Close();
        }

        protected void DropDownModule_SelectedIndexChanged(object sender, EventArgs e)
        {




        }

        protected void DropDownList3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}