﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="WebApplicationtest.WebForm2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <!DOCTYPE html>

    <html xmlns="http://www.w3.org/1999/xhtml">


    <body>
        <br />
        <br />
        <!-- Content here -->

        <div class="form">
            <div class="form-group">
                <label for="inputDevice">Device</label>
                <asp:DropDownList
                    ID="DropDownDevice"
                    runat="server"
                    AutoPostBack="true"
                    class="form-control"
                    Width="400px"
                    OnSelectedIndexChanged="DropDownDevice_SelectedIndexChanged">
                </asp:DropDownList>
            </div>

            <div class="form-group">
                <label for="inputModule">Module</label>
                <asp:DropDownList
                    ID="DropDownModule"
                    runat="server"
                    AutoPostBack="true"
                    class="form-control" OnSelectedIndexChanged="DropDownModule_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>

        <div class="form-group">
            <label for="inputType">Select Type</label>
            <asp:DropDownList
                ID="DropDownList3"
                runat="server"
                class="form-control">
                <asp:ListItem>AliceBlue</asp:ListItem>
                <asp:ListItem>AntiqueWhite</asp:ListItem>
            </asp:DropDownList>
        </div>

        <div class="form-group">
            <label for="inputQuestion">Question</label>
            <textarea id="inputQuestion" runat="server" cols="20" rows="2" placeholder="Apartment, studio, or floor" type="text" class="form-control"> </textarea>
        </div>



        <div class="form-group">

            <label for="inputAnswer">Answer</label>
            <textarea id="inputAnswer" cols="20" rows="2" placeholder="Apartment, studio, or floor" type="text" class="form-control"> </textarea>
        </div>

        <div>
            <p>Browse to Upload File</p>
            <asp:FileUpload ID="FileUpload1" runat="server" />
            <asp:Button ID="Button1" runat="server" Text="Upload File" OnClick="Buttonuplo_Click" />
        </div>
        <p>
            <asp:Label runat="server" ID="FileUploadStatus"></asp:Label>
        </p>

        <asp:Button ID="ButtonSave" runat="server" Text="Save" class="btn btn-primary" OnClick="ButtonSave_Click" />

        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label><br />

    </body>
    </html>

</asp:Content>
