﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplicationtest.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Home</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>


<body>

    <nav class="navbar navbar-dark bg-primary">
        <a class="navbar-brand">IT Helpdesk</a>
        <form class="form-inline">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-dark my-2 my-sm-0" type="submit">Search</button>
        </form>
    </nav>

    <form id="form1" runat="server">

        <br />

        <div class="container">
            <!-- Content here -->

            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputDevice">Select Device</label>
                    <asp:DropDownList
                        ID="DropDownDevice"
                        runat="server"
                        class="form-control">
                    </asp:DropDownList>
                </div>

                <div class="form-group col-md-6">
                    <label for="inputModule">Select Module</label>
                    <asp:DropDownList
                        ID="DropDownList2"
                        runat="server"
                        class="form-control">
                        <asp:ListItem>AliceBlue</asp:ListItem>
                        <asp:ListItem>AntiqueWhite</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group">
                <label for="inputType">Select Type</label>
                <asp:DropDownList
                    ID="DropDownList3"
                    runat="server"
                    class="form-control">
                    <asp:ListItem>AliceBlue</asp:ListItem>
                    <asp:ListItem>AntiqueWhite</asp:ListItem>
                </asp:DropDownList>
            </div>

            <div class="form-group">
                <label for="inputQuestion">Question</label>
                <textarea id="inputQuestion" runat="server" cols="20" rows="2" placeholder="Apartment, studio, or floor" type="text" class="form-control"> </textarea>
            </div>

            

            <div class="form-group">

                <label for="inputAnswer">Answer</label>
                <textarea id="inputAnswer" cols="20" rows="2" placeholder="Apartment, studio, or floor" type="text" class="form-control"> </textarea>
            </div>

            <div>
                <p>Browse to Upload File</p>
                <asp:FileUpload ID="FileUpload1" runat="server" />
                <asp:Button ID="Button1" runat="server" Text="Upload File" OnClick="Buttonuplo_Click" />
            </div>
            <p>
                <asp:Label runat="server" ID="FileUploadStatus"></asp:Label>
            </p>

            <asp:Button ID="ButtonSave" runat="server" Text="Save" class="btn btn-primary" OnClick="ButtonSave_Click" />
            
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label><br />
        </div>

    </form>












   
</body>
</html>
